import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RequestComponent } from './request/request.component';
import { RequestEditComponent } from './request/request-edit/request-edit.component';
import { RequestCreateComponent } from './request/request-create/request-create.component';
import { RequestTableComponent } from './request/request-table/request-table.component';
import { UnapprovedRequestsComponent } from './unapproved-requests/unapproved-requests.component';
import { EventComponent } from './event/event.component';
import { EventEditComponent } from './event/event-edit/event-edit.component';
import { EventCreateComponent } from './event/event-create/event-create.component';
import { EventTableComponent } from './event/event-table/event-table.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    // canActivate: [AuthGuard],
    children: [
      { path: 'unapproved', component: UnapprovedRequestsComponent },
      {
        path: 'events',
        component: EventComponent,
        children: [
          /*
          * TODO
          */
        ]
      },
      {
        path: 'requests',
        component: RequestComponent,
        children: [
          /*
          * TODO
          */
        ]
      },
      { path: '', redirectTo: 'unapproved', pathMatch: 'full' }
    ]
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { enableTracing: false }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

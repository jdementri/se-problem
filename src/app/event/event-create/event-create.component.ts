import { Component, OnInit } from '@angular/core';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { Router } from '@angular/router';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import { IEventForm } from 'src/app/interfaces';
import { IEvent } from 'projects/types/parking-requests/interfaces';
import * as moment from 'moment';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.scss']
})
export class EventCreateComponent implements OnInit {

  constructor(public es: EventService, public router: Router, public gms: GlobalMessageService) { }

  ngOnInit() {
  }

  createEvent(eventForm: IEventForm) {
    const newEvent: IEvent = {
      id: eventForm.id,
      eventName: eventForm.eventName,
      eventDate: '',
      eventTime: ''
    };

    const momentTime = moment.utc(eventForm.eventDateTime).local();
    newEvent.eventDate = momentTime.format('YYYY-MM-DD');
    newEvent.eventTime = momentTime.format('HH:mm:SS');

    this.es.createEvent(newEvent).then(() => {
      this.router.navigateByUrl('/home/events');
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to create event');
    });
  }

}

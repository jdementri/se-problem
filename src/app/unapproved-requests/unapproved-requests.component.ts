import { Component, OnInit } from '@angular/core';
import { IRequest, IEvent } from 'projects/types/parking-requests/interfaces';
import { RequestService } from 'projects/parking-database/src/lib/request/request.service';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { GlobalMessageService } from '../_services/global-message.service';
import { Router } from '@angular/router';

export interface IUnapprovedRequest {
  id: number;
  /**
   * TODO
   */
}

@Component({
  selector: 'app-unapproved-requests',
  templateUrl: './unapproved-requests.component.html',
  styleUrls: ['./unapproved-requests.component.scss']
})
export class UnapprovedRequestsComponent implements OnInit {

  unapprovedRequests: IUnapprovedRequest[];

  constructor(
    public rs: RequestService,
    public es: EventService,
    public gms: GlobalMessageService,
    public router: Router
  ) { }

  ngOnInit() {
    this.rs.getRequests().then(data => {
      // Only choose unapproved requests, and sort by Name
      const unapproved = data.filter(elem => elem.approved === false);
      unapproved.sort((a, b) => (a.name > b.name) ? 1 : -1);
      this.es.getEvents().then(e => {
        this.unapprovedRequests = this.setInitials(unapproved, e);
      }).catch(error => {
        console.error(error);
        this.gms.next('Unable to fetch events');
      });
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to fetch unapproved requests');
    });
  }

  private setInitials(req: IRequest[], evnts: IEvent[]): IUnapprovedRequest[] {
    const result: IUnapprovedRequest[] = [];
    req.forEach(elem => {
      const matchedEvent = evnts.find(e => e.id === elem.requestDateId);

      const currUnapprovedRequest: IUnapprovedRequest = {
        id: elem.id,
        /**
         * TODO
         */
      };

      result.push(currUnapprovedRequest);
    });
    // Sort by Date Desc
    return result;
  }

  editRequest(id: number) {
    this.router.navigateByUrl('/home/requests/edit/' + id);
  }

  approveRequest(req: IUnapprovedRequest) {
    this.rs.getRequestById(req.id).then(actualRequest => {
      actualRequest.approved = true;
      this.rs.updateRequest(actualRequest).then(() => {
        // Refresh
        this.ngOnInit();
      }).catch(error => {
        console.error(error);
        this.gms.next('Unable to approve request');
      });
    }).catch(error => {
      console.error(error);
      this.gms.next('Unable to find request with id = ' + req.id);
    });
  }


}

import { Component, OnInit, forwardRef, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { IgxDialogComponent } from 'igniteui-angular';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  // tslint:disable-next-line: component-selector
  selector: 'date-time-select',
  templateUrl: './date-time-select.component.html',
  styleUrls: ['./date-time-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateTimeSelectComponent),
      multi: true
    }
  ]
})
export class DateTimeSelectComponent implements ControlValueAccessor, OnInit {

  @ViewChild('dateTimeDialog', { static: false }) dateTimeDialog: IgxDialogComponent;
  // tslint:disable-next-line: no-input-rename
  @Input('placeholder') placeholder: string;
  // tslint:disable-next-line: no-input-rename
  @Input('defaultDate') defaultDate: string;

  isDisabled: boolean;
  displayValue: string;
  currentDate: Date;
  currentTime: Date;

  // tslint:disable-next-line: variable-name
  _value: string;
  get value(): string {
    return this.value;
  }
  @Input()
  set value(value: string) {
    this._value = value;
  }
  onChange: any = () => { };
  onTouched: any = () => { };

  constructor(public dp: DatePipe) { }

  ngOnInit() {
    if (this.defaultDate) {
      this.currentDate = new Date(moment(this.defaultDate).toISOString());
      this.currentTime = new Date(moment(this.defaultDate).toISOString());
    } else {
      this.currentDate = new Date(moment().toISOString());
      this.currentTime = new Date(moment().toISOString());
    }
  }

  openDateTimeDialog(event) {
    this.onTouched();
    this.dateTimeDialog.open();
  }

  selectDateTime(event) {
    const date = moment(this.currentDate).format('YYYY-MM-DD');
    const time = moment(this.currentTime).format('HH:mm:ss');
    const dateTime = moment(date + 'T' + time, 'YYYY-MM-DDTHH:mm:ss').toISOString();
    this.setDateTime(dateTime);
    this.setDisplayValue(dateTime);
    this.dateTimeDialog.close();
  }

  setDisplayValue(dateTimeValue: string) {
    this.displayValue = dateTimeValue ? this.dp.transform(dateTimeValue, 'h:mm a on EEEE, MMMM d, yyyy') : '';
  }

  setDateTime(dateTimeValue: string) {
    this.value = dateTimeValue;
    this.onChange(dateTimeValue);
  }

  writeValue(dateTimeValue: Date): void {
    const realDate = moment.utc(dateTimeValue).toISOString();
    this.setDateTime(realDate);
    this.setDisplayValue(realDate);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

}

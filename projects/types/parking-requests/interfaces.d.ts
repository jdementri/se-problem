declare namespace parkingRequests {
    export interface IRequest {
        id: number;
        name: string;
        date: string;
        comments: string;
        requestDateId: number;
        approved: boolean;
    }

    export interface IEvent {
        id: number;
        eventName: string;
        eventDate: string;
        eventTime: string;
    }
}

export = parkingRequests;
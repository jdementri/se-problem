import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IEvent } from 'projects/types/parking-requests/interfaces';
import { IEventForm } from 'src/app/interfaces';
import { IgxDialogComponent } from 'igniteui-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import * as moment from 'moment';

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.scss']
})
export class EventFormComponent implements OnInit {

  // tslint:disable-next-line: no-input-rename
  @Input('selectedEvent') selectedEvent: IEvent;
  // tslint:disable-next-line: no-output-rename no-output-on-prefix
  @Output('onSubmit') onSubmit = new EventEmitter<IEventForm>();

  @ViewChild('messageDialog', { read: IgxDialogComponent , static: false }) messageDialog: IgxDialogComponent;
  message: string;

  eventForm: FormGroup;

  constructor(
    public es: EventService,
    public fb: FormBuilder,
    public gms: GlobalMessageService
  ) { }

  ngOnInit() {
    this.setForm(this.selectedEvent);
  }

  setForm(event?: IEvent) {
    if (!event) {
      // No event => Create Form
      this.eventForm = this.fb.group({
        id: [null, Validators.required],
        eventName: ['', Validators.required],
        eventDateTime: ['', Validators.required]
      });
    } else {
      this.eventForm = this.fb.group({
        id: [event.id, Validators.required],
        eventName: [event.eventName, Validators.required],
        eventDateTime: [moment(new Date(event.eventDate + 'T' + event.eventTime)), Validators.required]
      });
      this.eventForm.controls.id.disable();
    }
  }

  submit(event) {
    this.onSubmit.emit(this.eventForm.value);
  }

}

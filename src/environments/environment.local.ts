export const environment = {
    production: false,
    type: 'dev',
    database: {
        url: 'http://localhost:3000'
    },
};

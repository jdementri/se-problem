import { TestBed } from '@angular/core/testing';

import { ScreenResizeEventService } from './screen-resize-event.service';

describe('ScreenResizeEventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ScreenResizeEventService = TestBed.get(ScreenResizeEventService);
    expect(service).toBeTruthy();
  });
});

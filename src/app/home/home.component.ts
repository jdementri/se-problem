import { Component, ViewChild, OnInit } from '@angular/core';


import {
  IgxSnackbarComponent,
  IgxDropDownComponent,
  IgxNavigationDrawerComponent,
  HorizontalAlignment,
  VerticalAlignment,
  ConnectedPositioningStrategy,
  CloseScrollStrategy,
  ISelectionEventArgs
} from 'igniteui-angular';
import { Router } from '@angular/router';
import { GlobalMessageService } from '../_services/global-message.service';
import { ScreenResizeEventService } from '../_services/screen-resize-event.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('snackbar', {static: true}) snackbar: IgxSnackbarComponent;
  @ViewChild(IgxDropDownComponent, {static: true}) igxDropDown: IgxDropDownComponent;
  @ViewChild(IgxNavigationDrawerComponent, {static: true}) navDrawer: IgxNavigationDrawerComponent;

  LOGOUT = 'logout';
  year: number;
  snackbarMsg: string;
  pages = [
    {
      link: '/home/',
      icon: 'home',
      name: 'Home'
    },
    {
      link: '/home/requests',
      icon: 'directions_car',
      name: 'Requests'
    },
    {
      link: '/home/events',
      icon: 'calendar_today',
      name: 'Events'
    }
  ];

  // tslint:disable-next-line: variable-name
  private _positionSettings = {
    horizontalDirection: HorizontalAlignment.Left,
    horizontalStartPoint: HorizontalAlignment.Right,
    verticalStartPoint: VerticalAlignment.Bottom
  };

  // tslint:disable-next-line: variable-name
  private _overlaySettings = {
    closeOnOutsideClick: true,
    modal: false,
    positionStrategy: new ConnectedPositioningStrategy(this._positionSettings),
    scrollStrategy: new CloseScrollStrategy()
  };

  constructor(
    public router: Router,
    public gms: GlobalMessageService,
    public sres: ScreenResizeEventService
  ) {
    this.year = new Date().getFullYear();
    this.gms.message$.subscribe(newMsg => {
      this.snackbarMsg = newMsg;
      this.snackbar.show();
    });
  }

  ngOnInit() {
    this.navDrawer.closed.subscribe(() => this.sres.next('closed'));
  }

  navigateTo(link: string): void {
    this.router.navigateByUrl(link);
  }

  toggleDropDown(eventArgs) {
    this._overlaySettings.positionStrategy.settings.target = eventArgs.target;
    this.igxDropDown.toggle(this._overlaySettings);
  }

  onSelection(e: ISelectionEventArgs) {
    const selectedValue = e.newSelection.value;
    if (selectedValue === this.LOGOUT) {
      this.logout();
    }
  }

  logout() {
    console.log('logging out');
    /**
     * TODO: Implement Auth Logout
     */
  }
}

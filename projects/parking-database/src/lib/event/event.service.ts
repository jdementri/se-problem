import { Injectable } from '@angular/core';
import { DatabaseService } from 'projects/database/src/lib/database.service';
import { DbLocatorService } from '../db-locator/db-locator.service';
import { IEvent } from 'projects/types/parking-requests/interfaces';

@Injectable({
  providedIn: 'root'
})
export class EventService {
  // tslint:disable-next-line: variable-name
  _endpoint = '/events';

  constructor(public ds: DatabaseService, public dbls: DbLocatorService) { }

  public async getEvents(): Promise<IEvent[]> {
    return this.ds.get(this.dbls.getLocation() + this._endpoint);
  }

  public async getEventById(id: number): Promise<IEvent> {
    return this.ds.get(this.dbls.getLocation() + this._endpoint + `/${id}`);
  }

  public async createEvent(event: IEvent): Promise<IEvent> {
    return this.ds.post(this.dbls.getLocation() + this._endpoint, event);
  }

  public async updateEvent(event: IEvent): Promise<any> {
    return this.ds.update(this.dbls.getLocation() + this._endpoint + `/${event.id}`, event);
  }

  public async deleteEvent(id: number): Promise<any> {
    return this.ds.delete(this.dbls.getLocation() + this._endpoint + `/${id}`);
  }
}

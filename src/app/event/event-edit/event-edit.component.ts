import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EventService } from 'projects/parking-database/src/lib/event/event.service';
import { GlobalMessageService } from 'src/app/_services/global-message.service';
import { IEventForm } from 'src/app/interfaces';
import { IEvent } from 'projects/types/parking-requests/interfaces';
import * as moment from 'moment';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent implements OnInit {

  selectedEvent;
  selectedId;

  constructor(public route: ActivatedRoute, public router: Router, public es: EventService, public gms: GlobalMessageService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      const id = +params.get('id');
      this.selectedId = id;
      this.es
        .getEventById(id)
        .then(e => {
          this.selectedEvent = e;
        })
        .catch(error => {
          console.error(error);
          this.gms.next('Could not load selected event');
        });
    });
  }

  updateEvent(eventForm: IEventForm) {
    const updatedEvent: IEvent = {
      id: this.selectedId,
      eventName: eventForm.eventName,
      eventDate: '',
      eventTime: ''
    };

    if (moment.isMoment(eventForm.eventDateTime)) {
      updatedEvent.eventDate = eventForm.eventDateTime.format('YYYY-MM-DD');
      updatedEvent.eventTime = eventForm.eventDateTime.format('HH:mm:SS');
    } else {
      const momentTime = moment.utc(eventForm.eventDateTime).local();
      updatedEvent.eventDate = momentTime.format('YYYY-MM-DD');
      updatedEvent.eventTime = momentTime.format('HH:mm:SS');
    }
    this.es
      .updateEvent(updatedEvent)
      .then(() => {
        this.gms.next('Updated event');
        this.router.navigateByUrl('/home/events');
      })
      .catch(error => {
        console.error(error);
        this.gms.next('Unable to update event');
      });
  }

}

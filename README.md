# Phillies Software Engineer Problem

In this repository, you will be presented with a relatively small application with a few pages that deal with a parking requests administration system. This style of app is very similar to other apps the Phillies Business Analytics department creates.

The purpose of this problem is to evaluate a candidate's ability to solve problems that a future Phillies Software Engineer would face.

There will be 3 issues with the code that we will ask you to fix.

1. We will need to set up the Angular Router. The variable `routes`, found in `app-routing.module.ts` on line 14, is the key that enables navigation from one component to the next as users perform actions within the app. Using all the components (which have already been imported into this file), organize a route structure using the correct paths with their components in the children area of the "events" Route Object and the "requests" Route Object. Note that the `RequestComponent.html` and `EventComponent.html` has the <router-outlet> tags in their HTML. This means that the Router will navigate to the next level down in the Route Tree, meaning these paths will require the children property to be filled out with the correct components. In addition, these will need empty paths that redirect to another component in the same level, so that the end user will not be redirected to a blank page (see examples). Please see the [Angular Documentation on Routing & Navigation for more details.](https://angular.io/guide/router)

2. Next, we need to complete the `UnapprovedRequestsComponent`. This component will act as the home screen for this app, where it will show cards for each request that has not been marked as approved by the user. Several things will need to be done here:

    *  Define the interface `IUnapprovedRequest`. This can be found starting on Line 8 of `unapproved-requests.component.ts`. This interface will be a combination of the IRequest interface, and the IEvent interface, which are found in projects/types/parking-requests/interfaces.d.ts.
    
        * Please make any date strings into Date objects (if date and time strings are separated, make them into one field and make that the Date type). This will help with displaying it in the HTML.

        * Add a field for initials. This field will be handled completely within this component.
        
    * Following the logic of the component, on it's initialization, it gets all unapproved requests, and all events, and passes both arrays to a helper method called setInitials(). Once in there, it loops over each request, find the matched event based on the requestDateId, and creates an IUnapprovedRequest. You will need to assign the fields of the IUnapprovedRequest, which starts on Line 50.
    
        * For the date fields, turn the strings into a Date Object. To learn more about the Date object, [see the Javascript Date object documentation.](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
        
        * Determine a way to find and set the initials for the name with the request.
        
    * Next, the results in the IUnapprovedRequest[] should be sorted by Date Descending. Please complete the implementation of the sorting. Note: you can choose to sort by either Date object, it is up to you. In addition, please keep it simple, sorting Dates is very easy in Javascript. This was one of the reasons we converted the date strings into Date objects earlier.
    

3. Ignite UI for Angular is a powerful and well designed extension that provides Material based UI widgets and components. This is used in almost every Phillies BA app as a way to present and manage data for our users. In this problem, we will need to set up the HTML side of it so that the Events (`event-table.component.html`) and Requests (`request-table.component.html`) data display in their grids properly. [See the Ignite Docs for instructions on how to set up the column configuration](https://www.infragistics.com/products/ignite-ui-angular/angular/components/grid/grid.html#columns-configuration). Please set it up so that under each comment is the correct column setup in the igx-grid for that field of the object we are trying to display in the grid.

There will only be two deliverables:

1. Your source code pushed to a private Gitlab repository and shared only with jdementri [at] phillies.com
2. A screenshot of the unapproved parking requests page. Once you have completed the problem, create a request under your name, so it will appear in the unapproved parking requests page.

## Dependencies

1. Node.Js v10.x or higher and NPM version 6.x or higher. ([Node & Angular Setup Guide](https://angular.io/guide/setup-local))
1. Latest Chrome or Safari browser

## Downloading/Installing the App

1. Clone the app
2. Run the following command

```bash
npm install
```

## Running the App

To run this app, you will need to open two terminal windows (Can be whatever terminal you prefer, or can be done right in VSCode). Navigate to the app root, and do the following steps:

1. Run the following command to create a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

```bash
ng serve
```

2. Run the following command for a local REST API using the data in the `db.json` file.

```bash
json-server --watch db.json
```

